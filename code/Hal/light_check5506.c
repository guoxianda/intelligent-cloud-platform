#include "light_check5506.h"



void light_check5506_init(void)
{
	HAL_ADCEx_Calibration_Start(&hadc1);
	HAL_Delay(200);
}

uint32_t light_check5506_getinitvalue(void)
{
	HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1,50);//限时50ms
	return HAL_ADC_GetValue(&hadc1);
}

uint32_t light_check5506_get0to100value(void)
{
	//暗-->亮：0~100
	uint32_t value;
	value=light_check5506_getinitvalue();
	value=4096-value;//原始数据是越暗数据越大
	value=(value*100/4096);//化为0~100的数,必须先乘以100再除，因为全部是整数
	return value;
}

void Test_5506(void)
{
	uint32_t value;
	char txt[16];
	while(1)
	{
		value=light_check5506_get0to100value();
		sprintf(txt, "light(0-100):%d", value);  
		OLED_ShowString(0,0,txt);
		OLED_Refresh_Gram();
	}
}
