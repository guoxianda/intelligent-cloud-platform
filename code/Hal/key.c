#include "key.h"
#include "oled.h"
#include "gizwits_protocol.h"

void key_init(void)
{
	HAL_GPIO_WritePin(KEY_COM_GND_GPIO_Port,KEY_COM_GND_Pin, GPIO_PIN_RESET);
}


void Test_key(void)
{
	if(HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin)==GPIO_PIN_SET)
	{
		OLED_ShowString(0,0,"key1_up");
	}
	else
	{
		OLED_ShowString(0,0,"key1_down");
	}
	if(HAL_GPIO_ReadPin(KEY2_GPIO_Port, KEY2_Pin)==GPIO_PIN_SET)
	{
		OLED_ShowString(0,10,"key2_up");
	}
	else
	{
		OLED_ShowString(0,10,"key2_down");
	}
	OLED_Refresh_Gram();	
}

void key_wait_set_wifi(void)
{
//	while(1)
//	{	
//		if(HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin)!=GPIO_PIN_SET)//按下
//		{
//			HAL_Delay(20);//消抖
//			while(HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin)==GPIO_PIN_SET){}//直到按键弹开
//			break;
//		}
//	}
	while(HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin)==GPIO_PIN_SET){}
	gizwitsSetMode(WIFI_AIRLINK_MODE);
}