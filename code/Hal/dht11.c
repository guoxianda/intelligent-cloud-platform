#include "dht11.h"

//DHT11输出模式配置
void DHT11_IO_OUT()	
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.Pin=DHT11_Pin;
	GPIO_InitStructure.Mode=GPIO_MODE_OUTPUT_PP;	 //推挽输出
	GPIO_InitStructure.Speed=GPIO_SPEED_FREQ_LOW;	
	HAL_GPIO_Init(DHT11_GPIO_Port, &GPIO_InitStructure);
}
 
//DHT11输入模式配置
void DHT11_IO_IN()	
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.Pin=DHT11_Pin;
	GPIO_InitStructure.Mode=GPIO_MODE_INPUT;	 //上拉输入模式
	GPIO_InitStructure.Pull=GPIO_PULLUP;
	HAL_GPIO_Init(DHT11_GPIO_Port, &GPIO_InitStructure);
}
void delay_us(uint32_t us)
{
	//HAL_RCC_GetHCLKFreq()为16M，每一个while（delay--）需要4个时钟
	//16M/4M/4=1us
    uint32_t delay = (HAL_RCC_GetHCLKFreq() / 4000000 * us);
    while (delay--)
	{
		;
	}
}

//void delay_us(uint32_t us)
//{
//		__HAL_TIM_SetCounter(&htim3, 0);//htim3

//		__HAL_TIM_ENABLE(&htim3);

//		while(__HAL_TIM_GetCounter(&htim3) < us);
//		/* Disable the Peripheral */
//		__HAL_TIM_DISABLE(&htim3);
//}



//复位DHT11
void DHT11_Rst(void)
{
    DHT11_IO_OUT(); 	//SET OUTPUT
    DHT11_DQ_OUT=0; 	//拉低DQ
    HAL_Delay(20);    	//拉低至少18ms,(DHT22 500us)
    DHT11_DQ_OUT=1; 	//DQ=1
    delay_us(30);     	//主机拉高20~40us
}

//等待DHT11的回应
//返回1:未检测到DHT11的存在
//返回0:存在
uint8_t DHT11_Check(void)
{
    uint8_t retry=0;
    DHT11_IO_IN();//SET INPUT
    while (DHT11_DQ_IN&&retry<100)//DHT11会拉低40~80us
    {
        retry++;
        delay_us(1);
    };
    if(retry>=100)return 1;
    else retry=0;
    while (!DHT11_DQ_IN&&retry<100)//DHT11拉低后会再次拉高40~80us
    {
        retry++;
        delay_us(1);
    };
    if(retry>=100)return 1;
    return 0;
}

//从DHT11读取一个位
//返回值：1/0
uint8_t DHT11_Read_Bit(void)
{
    uint8_t retry=0;
    while(DHT11_DQ_IN&&retry<100)//等待变为低电平
    {
        retry++;
        delay_us(1);
    }
    retry=0;
    while(!DHT11_DQ_IN&&retry<100)//等待变高电平
    {
        retry++;
        delay_us(1);
    }
    delay_us(40);//等待40us
    if(DHT11_DQ_IN)return 1;
    else return 0;
}

//从DHT11读取一个字节
//返回值：读到的数据
uint8_t DHT11_Read_Byte(void)
{
    uint8_t i,dat;
    dat=0;
    for (i=0; i<8; i++)
    {
        dat<<=1;
        dat|=DHT11_Read_Bit();
    }
    return dat;
}

//从DHT11读取一次数据
//temp:温度值(范围:0~50°)
//humi:湿度值(范围:20%~90%)
//返回值：0,正常;1,读取失败
uint8_t DHT11_Read_Data(uint8_t *humi_integer,uint8_t *humi_decimal,uint8_t *temp_integer,uint8_t *temp_decimal)
{
    uint8_t buf[5];
    uint8_t i;
    DHT11_Rst();
    if(DHT11_Check()==0)
    {
        for(i=0; i<5; i++) //读取40位数据
        {
            buf[i]=DHT11_Read_Byte();
        }
        if((buf[0]+buf[1]+buf[2]+buf[3])==buf[4])
        {
						*humi_integer=buf[0];
						*humi_decimal=buf[1];
						*temp_integer=buf[2];
						*temp_decimal=buf[3];
        }
    } else return 1;
    return 0;
}

uint8_t DHT11_Read_Data_Float(float *temp,float *humi)
{
    uint8_t buf[5];
    uint8_t i;
    DHT11_Rst();
    if(DHT11_Check()==0)
    {
        for(i=0; i<5; i++) //读取40位数据
        {
            buf[i]=DHT11_Read_Byte();
        }
        if((buf[0]+buf[1]+buf[2]+buf[3])==buf[4])
        {
			*humi=((buf[0] << 8) + buf[1]) / 10.0;
			*temp=((buf[2] << 8) + buf[3]) / 10.0;
        }
    } else return 1;
    return 0;
}

//初始化DHT11的IO口 DQ 同时检测DHT11的存在
//返回1:不存在
//返回0:存在
uint8_t DHT11_Init(void)
{
    uint8_t ret = 1;
    DHT11_Rst();  //复位DHT11
    ret = DHT11_Check();
    return ret;
}

void Test_dht11(void)
{
	char txt[16];
	while(1)
	{
		DHT11_Read_Data(&humidity_integer,&humidity_decimal,&temperature_integer,&temperature_decimal);				 
		sprintf(txt, "temp:%d.%d", temperature_integer,temperature_decimal);  
		OLED_ShowString(0,0,txt);
		sprintf(txt, "humi:%d.%d", humidity_integer,humidity_decimal);  
		OLED_ShowString(0,10,txt);
		OLED_Refresh_Gram();	
	}
}