#ifndef __DHT11_H
#define __DHT11_H

#include "main.h"
#include "sys.h"
#include "tim.h"


extern uint8_t temperature_integer;         
extern uint8_t humidity_integer; 
extern uint8_t temperature_decimal;         
extern uint8_t humidity_decimal;


//IO操作函数
#define	DHT11_DQ_OUT PBout(0) //数据端口	
#define	DHT11_DQ_IN  PBin(0)  //数据端口	

uint8_t DHT11_Init(void);//初始化DHT11
uint8_t DHT11_Read_Data(uint8_t *humi_integer,uint8_t *humi_decimal,uint8_t *temp_integer,uint8_t *temp_decimal);//读取温湿度
uint8_t DHT11_Read_Byte(void);//读出一个字节
uint8_t DHT11_Read_Bit(void);//读出一个位
uint8_t DHT11_Check(void);//检测是否存在DHT11
void DHT11_Rst(void);//复位DHT11
uint8_t DHT11_Read_Data_Float(float *temp,float *humi);
void Test_dht11(void);
	
#endif

