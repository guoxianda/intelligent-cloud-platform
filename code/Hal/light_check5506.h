#ifndef __LIGHT_CHECK5506_H
#define __LIGHT_CHECK5506_H
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "oled.h"

void light_check5506_init(void);
uint32_t light_check5506_getinitvalue(void);
uint32_t light_check5506_get0to100value(void);
void Test_5506(void);
#endif



