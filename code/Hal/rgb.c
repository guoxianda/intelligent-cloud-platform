#include "rgb.h"
#include "tim.h"



void rgb_init(void)
{
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_3);
}



void Test_rgb(void)
{
    rgb_setpwm(10.0,100.0,200.0);//依次是RGB各自的分量
}

void rgb_setpwm(uint8_t pwm_r,uint8_t pwm_g,uint8_t pwm_b)
{
	__HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_1,pwm_r);
	__HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2,pwm_g);
	__HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_3,pwm_b);

}