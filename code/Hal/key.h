#ifndef __KEY_H
#define __KEY_H
#include "stm32f1xx_hal.h"

void key_init(void);
void Test_key(void);
void key_wait_set_wifi(void);
#endif